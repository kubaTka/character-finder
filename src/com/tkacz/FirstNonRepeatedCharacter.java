package com.tkacz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//
public class FirstNonRepeatedCharacter {


    public char findFirstChar(String s) {
        Map<Character, Integer> couters = new HashMap<>();
        List<Character> characters = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            final Integer currentCount = couters.get(c);
            if (currentCount == null) {
                couters.put(c, 1);
                characters.add(c);
            } else {
                couters.put(c, currentCount + 1);
            }
        }
        for (char c : characters) {
            final Integer integer = couters.get(c);
            if(integer==1){
              return c;
            }
        }
        throw new RuntimeException("char not found");
    }

    public char findFirstChar2(String s) {

        for (int i = 0; i < s.length(); i++) {
            char charToFind = s.charAt(i);

            int count = 0;
            for (int j = 0; j < s.length(); j++) {
                if (s.charAt(j) == charToFind) {
                    count++;
                }
            }
            if (count == 1) {
                return charToFind;
            }

        }
        throw new RuntimeException("char not found");

    }

    public static void main(String[] args) {
        FirstNonRepeatedCharacter main = new FirstNonRepeatedCharacter();
        final StringBuilder stringBuilder = new StringBuilder(100000);
        for (int i = 0; i < 100000; i++) {
            stringBuilder.append("0123456789");

        }
        stringBuilder.append("a");
        System.out.println(main.findFirstChar2(stringBuilder.toString()));

    }

}